# lancement du playbook ansible
resource "null_resource" "ansible" {
  provisioner "local-exec" {
    interpreter = ["/bin/bash", "-c"]
    #command = "source /home/user/TP/Ansible/venv/ansible/bin/activate; ANSIBLE_HOST_KEY_CHECKING=False ansible -i ../Ansible/hosts.ini all -m ping --private-key .ssh/${var.INSTANCE_BASTION_KEY_PAIR}.pem -e pub_key=.ssh/${var.INSTANCE_BASTION_KEY_PAIR}.pub; deactivate"
    #command = "source /home/user/TP/Ansible/venv/ansible/bin/activate; ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../Ansible/hosts.ini ../Ansible/deploy.yml; deactivate"
    command = "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook -i ../Ansible/hosts.ini ../Ansible/deploy.yml"
  }
  depends_on = [
    openstack_compute_instance_v2.bastion_instance,
    openstack_compute_instance_v2.orchestration_instance,
    null_resource.copy_keypair_cluster,
    null_resource.copy_local_ip_bastion
  ]
}
